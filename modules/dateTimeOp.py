#datetiem manage 
from datetime import datetime
from datetime import timedelta
import datetime 
import time

# global printlog

# formats examples:
# file extension str_datetime_format('-%Y-%m-%d-%H-%M-%S') '-%Y-%m-%d-%H-%M-%S'
def str_datetime_format(ff): # for unique files names
	dt=datetime.datetime.now()
	# print('dt',dt,ff)
	# print('dt2',dt.strftime(ff))
	return str(dt.strftime(ff))



class Timer:
	def __init__(self):
		self.start()
	
	def start(self):
		self.start=time.time()

	def end(self):
		self.end=time.time()
		
	
	def tick(self,units='s',prec=4): # units: s, m, h,d
	
		tmp= time.time()-self.start
		if units=='m':
			tmp=round(tmp/60.0,prec)
		elif units=='h':
			tmp=round(tmp/3600.0,prec)
		elif units=='d':
			tmp=round(tmp/86400.0,prec)
		
		return tmp # print(custStr,tmp)