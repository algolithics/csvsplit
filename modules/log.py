# printGeneralized
# goal:
# print to the screen + save to fle log 

from modules.dateTimeOp import str_datetime_format

class Log:
	
	def __init__(self,mode='append'): # append or 'new'
		self.lines=[]
		self.logname='z_log.txt' # if append continue in the same file 
		if mode=='new':
			self.logname='z_log_'+str_datetime_format('%Y_%m_%d_%H_%M_%S')+'.txt'
		
		# self.write_file=open
		# self._open=open;
		
	def log(self,*txt):
		print(*txt)
		newlinereplace='\n                       '
		txt=' '.join([str(el).replace('\n',newlinereplace) for el in txt])
		dt=str_datetime_format('%Y-%m-%d-%H:%M:%S')
		self.lines.append({dt:txt})
		self.saveLine(dt,txt)
		
		
	def saveLine(self,dt,txt):	
		with open(self.logname,'a') as f:
			f.write(str(dt)+' : '+str(txt)+'\n')
			
			
			
	# old version better use log()	
	def addLine(self,txt,displ=True):
	
		if displ:
			if type(txt)==type( (1,) ):
				print(*txt)
			else:
				print(txt)
					
		newlinereplace='\n                       '
		if type(txt)==type( (1,) ):
			txt=' '.join([str(el).replace('\n',newlinereplace) for el in txt])
	
			
		dt=str_datetime_format('%Y-%m-%d-%H:%M:%S')
		self.lines.append({dt:txt})
		
		self.saveLine(dt,txt)
		
	
			
	
					