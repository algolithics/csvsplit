#fileOp
# save_df( )
# saveDictList(self,fname,dictList)
# xlsx_read(filename )
# printDataFrameAnalysis(df,rows=3)
# csv_read(filename, cur_cfg)
# getFilePaths(mainDir,file_pattern='')
# FilePath class 
# zip/tar class
# jsonFromStr(json_str)
# clearComments(txt,comment_char='#')
# trimJsonStr
# fixJson
# jsonFromFile
# jsonToStr
# jsonToFile

import tarfile # py built in 
import subprocess, os
import traceback
from pandas import DataFrame, read_csv
from itertools import islice 
from openpyxl import load_workbook
import ujson as json #

global printlog




#
# JSON PROCESSING
#

# exception when too much coma or coma missing between values 
def jsonFromStr(json_str):
	try:
		return json.loads(json_str)
	except:
		err=traceback.format_exc()
		printlog.log(err )
		printlog.log(json_str )
		return {'err':err}
		
		
# clean comments
def clearComments(txt,comment_char='#'):
	newtxt=''
	lines=txt.split('\n') 
	for ll in lines:
		lls=ll.strip() # remove whitespace 
		if len(lls)==0 or lls[0]==comment_char:
			continue
		else:
			ii=lls.find(comment_char)
			if ii>-1:
				lls=lls[0:ii] 
		newtxt+=lls+'\n' 
	return newtxt
	
	
		
# del all before first / lasst [{ }]
def trimJsonStr(jstr):

	start=len(jstr)-1
	for ss in ['{','[']:
		tmps=jstr.find(ss)
		if tmps>-1 and tmps<start:
			start=tmps
	 
	end=0
	for ss in ['}',']']:
		tmps=jstr.rfind(ss)
		if tmps>-1 and tmps>end:
			end=tmps 
	
	return jstr[start:end+1]
	
	
	
def fixJson(jstr):
	return jstr.replace("'",'"').replace("True",'true').replace("False",'false')
	
		
def jsonFromFile(fpath):
	if not os.path.exists(fpath):
		printlog.log('Cannot read json - path does not exist:'+fpath )
		return
		
	json_str=''
	with open(fpath, "rb") as ff:
		json_str = ff.read().decode("utf-8")
		
	json_str=fixJson(trimJsonStr(json_str))
	json_str=clearComments(json_str)
		
	return jsonFromStr(json_str)
	
	
	
		
def jsonToStr(jdata):
	try:
		return json.dumps(jdata) 
	except:
		err=traceback.format_exc()
		printlog.log(err)
		return err
		
		
		

def jsonToFile(jdata,fpath,overwrite=False):
	try:
		if not overwrite and os.path.exists(fpath):
			printlog.log('Path '+fpath+' exists - not writing!')
			return
		
		json_str= jsonToStr(jdata)
		json_bytes = json_str.encode('utf-8')
		with open(fpath, "wb") as fout: 
			fout.write(json_bytes)
		return True
	except:
		err=traceback.format_exc()
		printlog.log(err) 
		return False








class Zip: # 7zip or tar 
	

	def __init__(self ): 
	
		#
		# CHECK ZIP OPT AVAILABLE
		# 
		self.zipOn=False
		self.init7zpath='7z'
		try: #7zFM
			tmp=subprocess.call([self.init7zpath, 'a', '-y', 'test.zip', 'modules/fileOp.py']  , shell=True, stdout=subprocess.PIPE)
			tmpwarn=('please install 7z to allow zipping result files',)
			if tmp!=0:
				self.init7zpath='C:\\Program Files\\7-Zip\\7z'
				tmp=subprocess.call([self.init7zpath, 'a', '-y', 'test.zip', 'modules/fileOp.py']  , shell=True, stdout=subprocess.PIPE)
				if tmp==0:
					tmpwarn=('please set proper PATH environment for 7z ',)
					printlog.addLine( tmpwarn )
			# print(tmp)
			if tmp==0: # ok delete the file
				self.zipOn=True
				os.remove('test.zip')
			else: # zip off
				printlog.addLine( tmpwarn )
				self.init7zpath='bad path for 7z'
				# print('please install 7z to allow zipping result files') 
		except:
			printlog.addLine( (traceback.format_exc(),) )
			
		
	def saveZipDelOrig(self,zip_file, fname):
		if self.zipOn==False:
			return False
		
		subprocess.call([self.init7zpath, 'a', '-y', zip_file, fname]  , shell=True)
		os.remove(fname)
		
		return True
		
		
		
		
	def tar(self,fname,paths):
	
		if '.tar' not in fname:
			fname+='.tar' 
			
		with tarfile.open(fname ,"w") as tfile:
			for pp in paths: #Adding files to the tar file
				tfile.add(pp)
	#
	# FUN TO ZIP ; path_list with wildcard 
	# if not available - use tar
	# 
	def zip_files(self,zip_fname,path_pattern): #,remove_orig=True):
		
		# self.zipOn=False # test 
		if self.zipOn==False: #use tar instead
			
			printlog.log( '7zip not installed or not proper PATH - using tar instead of zip' )
			
			fp=FilePath(path_pattern) # split dir and file name since pattern may have dir before file pattern 
			
			mainDir='.'
			if len(fp.dir_path)>0:
				mainDir=fp.dir_path[0]
				
			paths=getFilePaths(mainDir,file_pattern=fp.file_name)
			
			self.tar(zip_fname,paths)
			
			return True 
			
		if '.zip' not in zip_fname:
			zip_fname+='.zip' 
			
		subprocess.call([self.init7zpath, 'a', '-y', zip_fname, path_pattern]   , shell=True)
		return True
		
		

class FilePath:

	def __init__(self,filePath):
		self.path_separator=''
		self.dir_path=[]
		self.file_name=''
 
		if '\\' in filePath:
			self.path_separator='\\'

		elif '/' in filePath: 
			self.path_separator='/'
			
			
		if self.path_separator=='':
			printlog.addLine( ('\n*** Be aware of Windows path separator - should be either "/" or "\\\\", not single "\\" \n',) )
			self.file_name=filePath
		else:
			filePath=filePath.split(self.path_separator)
		
			self.dir_path=filePath[:-1]
			self.file_name=filePath[-1]
			
			

# example asdf*.txt * is wild card 
def getFilePaths(mainDir,file_pattern=''):
	paths=[]
	pat_split=file_pattern.split('*')
	l1=0
	l2=0
	if file_pattern!='':
		l1=len(pat_split[0])
		if len(pat_split)>1:
			l2=len(pat_split[1])
		
	for root, dirs, files in os.walk(mainDir):
		for file in files:
			tmpflen=len(file)
			# print('testing file',file)
			# exit()
			if file_pattern!='' :
				if l1>0 and l2>0 :
					if pat_split[0]==file[:l1] and pat_split[1]==file[tmpflen-l2:]:
						paths.append(os.path.join(root,file))
						# print('c1')
				elif l1>0:
					if pat_split[0]==file[:l1]:
						paths.append(os.path.join(root,file))
						# print('c2',l1,pat_split[0],file[:l1])
				elif l2>0 :
					if pat_split[1]==file[tmpflen-l2:]:
						paths.append(os.path.join(root,file))
						# print('c3',l2,pat_split[1],file[tmpflen-l2:])
			else:
				paths.append(os.path.join(root,file))
	# print(paths)		
	return paths
	
	

def printDataFrameAnalysis(df,rows=3):
	printlog.addLine(('\n======================File analysis:',) )
	printlog.addLine(('==top 3 rows',) )
	printlog.addLine((df.head(rows),'\n') )
	printlog.addLine(('==column types',) )
	printlog.addLine((df.dtypes,'\n') )




def csv_read(filename, cur_cfg):
	
	do_infer_datetime_format=False
	if len(cur_cfg["csv_dates_columns_to_parse"])>0:
		do_infer_datetime_format=True
	try:
		df=read_csv(filename, parse_dates=cur_cfg["csv_dates_columns_to_parse"],dayfirst=cur_cfg["csv_us_format_day_first"], delimiter=cur_cfg["csv_delimiter"],   dtype=cur_cfg["csv_dtype"], infer_datetime_format=do_infer_datetime_format)
		return df
	except:
		printlog.addLine(('Could not read ',file_type,'file',filename) )
		tmp=traceback.format_exc()
		
		if 'ValueError:' in (tmp):
			tmp2=tmp.split('ValueError:')
			printlog.addLine(('Wrong column name to parse date? or dtype?',tmp2[1]) )
		else:
			printlog.addLine((tmp,) )
		
		return None



def xlsx_read(filename ):
 
	wb = load_workbook(filename )
	ws=wb[wb.sheetnames[0]] 
	
	data = ws.values
	cols = next(data)[0:]  
	data = list(data) 
	data = (islice(r, 0, None) for r in data) 
	 
	df = DataFrame(data,index=None, columns=cols)  # df2 = df[xls_col_sel]
			
	return df


#
# FUN TO SAVE CSV ;
# fname - name of file, csv_fdir - dir containing file 
def save_df(csv_fname, csv_fdir, df_save, save_col_names,zipObj, only_save_zip=True):
# def save_df(csv_fname, df_save, cur_cfg,zipObj, only_save_zip=True):
	if not df_save.empty:
		fname=csv_fname
		if not save_col_names: # then add to file name 
			# print('not save_col_names',df_save.columns)
			for cc in df_save.columns: #
				if  len(fname)+4+1+len(cc)<128:
					fname+='-'+cc
					# print('fname',fname)
				else:
					break
				
		# fname=csv_fname+'.csv'
		fpath=os.path.join(csv_fdir, fname+'.csv' )
		if save_col_names:
			df_save.to_csv( fpath, sep=';', encoding='utf-8', index=False, header=df_save.columns)
		else:
			df_save.to_csv( fpath, sep=';', encoding='utf-8', index=False, header=None)
			
			
		if zipObj.zipOn and only_save_zip:
			zip_file=os.path.join(csv_fdir, fname+'.zip' ) #csv_fname+'.zip'
			zipObj.saveZipDelOrig(zip_file, fpath)
			return zip_file
			
		return fpath
	else:
		printlog.addLine(('empty df',) )
		return ''
		
		

#
#  SAVE dict list [{},{}]
# 	
def saveDictList(self,fname,dictList): 
		
	with open(fname,'w') as f:
		for dd in dictList:
			for k,v in dd.items(): 
				f.write(str(k)+' : '+str(v)+'\n')