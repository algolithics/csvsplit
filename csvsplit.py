# This script will read csv of xls file (first sheet)
# to split into many files
# based on category specified and max number of rows specified in the config 

# OPTIONALLY REQUIRES 7z installed for zipping - if not - will create many files in the result folder 



# python -m pip install pyinstaller # to create own exec:
# pyinstaller --onefile csvsplit.py

import os 
import modules.log
import modules.fileOp as fop 
from modules.dateTimeOp import str_datetime_format

import traceback
		

global printlog
printlog=modules.log.Log()
fop.printlog=printlog 


config_dict=fop.jsonFromFile('config.py')




#
# PROCESS CONFIG FILE
# default config:

cur_cfg= {
	"csv_dates_columns_to_parse": [],
	"csv_us_format_day_first": False,
	"csv_delimiter": ';', 
	"csv_dtype": {},

	"filename":  '',
	"selected_columns_to_save": ['id'],
	"split_category_column": '',				
	"max_rows_per_file": 1024*1024*1024,
	"save_column_names": True ,
	"zip":True
}

must_have_keys=["filename","selected_columns_to_save","split_category_column","max_rows_per_file","save_column_names","zip"]
keys_missing=must_have_keys.copy()
for k,v in config_dict.items(): # cfg. copy config values 
	if k in cur_cfg:
		cur_cfg[k]=v 
		if k in keys_missing:
			keys_missing.remove(k)
	else:
		printlog.addLine( ('Wrong option name [',k,']\nAllowed:\n',list(cur_cfg.keys()) ) ) 
		exit()
	
if len(	keys_missing)>0:
	printlog.log('!!! Must have keys missing:',keys_missing)
	exit()

	
#
# VALIDATE CONFIG FILE
# 
if type(cur_cfg["max_rows_per_file"])!=type(1) or cur_cfg["max_rows_per_file"]<1:
	printlog.addLine( ('Wrong value in ',"max_rows_per_file",cur_cfg["max_rows_per_file"],'must be integer >0') )
	exit()
if type(cur_cfg["save_column_names"])!=type(True)  :
	printlog.addLine( ('Wrong value in ',"save_column_names",cur_cfg["save_column_names"],'type',type(cur_cfg["save_column_names"]),'must be boolean True or False') )
	exit()
if type(cur_cfg["zip"])!=type(True)  :
	printlog.addLine( ('Wrong value in ',"zip",cur_cfg["zip"],'type',type(cur_cfg["zip"]),'must be boolean True or False') )
	exit()
if type(cur_cfg["split_category_column"])!=type('')  :
	printlog.addLine( ('Wrong value in ',"split_category_column",cur_cfg["split_category_column"],'must be string, column name to use as category to split file') )
	exit()
if type(cur_cfg["selected_columns_to_save"])!=type([]) or type(cur_cfg["selected_columns_to_save"][0])!=type('') :
	printlog.addLine( ('Wrong value in ',"selected_columns_to_save",cur_cfg["selected_columns_to_save"],'must be list of strings: column names to save') )
	exit()
if type(cur_cfg["filename"])!=type('') or not os.path.exists(cur_cfg["filename"]) :
	printlog.addLine( ('Wrong value in ',"filename",cur_cfg["filename"],'must be string, name of file to read ') )
	if not os.path.exists(cur_cfg["filename"]):
		printlog.addLine( ('no such file') )
	exit()
	



#
# CONFIG values to local variables 
# 
filename= cur_cfg["filename"]
selected_columns=cur_cfg["selected_columns_to_save"]
split_category_column=cur_cfg["split_category_column"]
max_rows_per_file=cur_cfg["max_rows_per_file"]
save_column_names=cur_cfg["save_column_names"]


#
# create resut folder if not exist
# 
if not os.path.exists('results'):
	os.makedirs('results')
	
	
	

fname_split=filename.split('.')
orig_file_name=fname_split[0]


#
# CHECK PROPER FILE PATH HANDLING / windows exception
# 
fp=fop.FilePath( orig_file_name)

subdir=fp.file_name+ str_datetime_format('-%Y-%m-%d-%H-%M-%S')
subdir_result=os.path.join('results',subdir)

if not os.path.exists(subdir_result):
	os.makedirs(subdir_result)


#
# ANALIZE FILE FORMAT
# 
file_type='xlsx'
if fname_split[-1].lower()==file_type:
	printlog.addLine( ('\nDetected file type Excel') )
elif fname_split[-1].lower()=='csv':
	file_type='csv'
	printlog.addLine( ('\nDetected file type csv - using defined csv settings:',) )
	printlog.addLine( ('csv_dates_columns_to_parse value:',cur_cfg["csv_dates_columns_to_parse"]) )
	printlog.addLine( ('csv_us_format_day_first value:',cur_cfg["csv_us_format_day_first"]) )
	printlog.addLine( ('csv_delimiter value:',cur_cfg["csv_delimiter"]) )
	printlog.addLine( ('csv_dtype value:',cur_cfg["csv_dtype"] ) )
	
	# parse csv type:
	parse_types={'str':type('str'), 'int':type(1), 'float':type(1.1)}
	to_remove=[]
	for k,v in cur_cfg["csv_dtype"].items():
		# print('parsing',k,v,type(v))
		if v not in parse_types:
			printlog.addLine( ('### wrong csv_dtype value will be removed :',k,v,'allowed', parse_types) )
			
			to_remove.append(k)
		else:	
			cur_cfg["csv_dtype"][k]=parse_types[v]
			
	for rr in to_remove:
		del cur_cfg["csv_dtype"][rr]
	 #
	# VALIDATE CONFIG FILE - CSV part 
	# 
	if file_type=='csv' and type(cur_cfg["csv_dates_columns_to_parse"])!=type([]) or (len(cur_cfg["csv_dates_columns_to_parse"])>0 and type(cur_cfg["csv_dates_columns_to_parse"][0])!=type('')) :
		printlog.addLine( ('Wrong value in ',"csv_dates_columns_to_parse",cur_cfg["csv_dates_columns_to_parse"],'must be list of strings: column names to parse dates') )
		# printlog.addLine( (type(cur_cfg["csv_dates_columns_to_parse"][0]) , type(''))
		exit()	
	if file_type=='csv' and type(cur_cfg["csv_us_format_day_first"])!=type(True)   :
		printlog.addLine( ('Wrong value in ',"csv_us_format_day_first",cur_cfg["csv_us_format_day_first"],'type',type(cur_cfg["csv_us_format_day_first"]),'must be True or False') )
		exit()	
	if file_type=='csv' and cur_cfg["csv_delimiter"] not in [';',',','\t','\n','|']   :
		printlog.addLine( ('Wrong value in ',"csv_delimiter",cur_cfg["csv_delimiter"],'must be in',[';',',','\t','\n','|']) )
		exit()
	if file_type=='csv' and type(cur_cfg["csv_dtype"])!=type({}) :
		printlog.addLine( ('Wrong value in ',"csv_dtype",cur_cfg["csv_dtype"],'must be in dictionary, may be empty eg.: {}' ) )
		exit()
 
 
 
 




#
# READ AND ANALIZE DATAFRAME from CSV or XLS
# 
df=None
try:
	if file_type=='xlsx':
		df= fop.xlsx_read(filename )
	else:
		df= fop.csv_read(filename, cur_cfg )
		
except:
	printlog.addLine( (traceback.format_exc()) )
	printlog.addLine( ('Could not read ',file_type,'file',filename) )
	# exit()
	
# print(type(None))
if type(df)==type(None):
	exit()
	

#
# DATAFRAME SUMMARY
# 	
# print(df.head(3))
if split_category_column!=None and split_category_column!='':
	df=df.astype({split_category_column: 'category'})
		
fop.printDataFrameAnalysis(df)






#
# PROCESS FILE AND SAVE MULTI FILES
# 

df_by_cat={}

if split_category_column!=None and split_category_column!='':

	categories=df[split_category_column].unique() #.sort()
	printlog.addLine( ( len(categories),'categories of dataframe rows',len(df[split_category_column]),'for column',split_category_column,'\n') )
	printlog.addLine( (categories)  )
	
	for cc in categories: 
		tmpdf=df.loc[ df[split_category_column] == cc ]
		df_shape=tmpdf.shape
		
		start=0
		end= min(max_rows_per_file,df_shape[0])
		df_by_cat[cc]={}
		while start<df_shape[0]:
		
			tmpdfii=tmpdf.iloc[start:end] # df.iloc[:3]
		
			df_by_cat[cc][str(start)+'-'+str(end-1)]=tmpdfii
			start+=max_rows_per_file
			end+=max_rows_per_file
			end=min(end,df_shape[0]) 
else:
	  
	df_shape=df.shape 
	start=0
	end= min(max_rows_per_file,df_shape[0])
	df_by_cat['nocat']={}
	while start<df_shape[0]:
	
		tmpdfii=df.iloc[start:end] # df.iloc[:3]
	
		df_by_cat['nocat'][str(start)+'-'+str(end-1)]=tmpdfii
		start+=max_rows_per_file
		end+=max_rows_per_file
		end=min(end,df_shape[0]) 




zipo=fop.Zip()
path_to_zip=[] # actually path to remove after zip, zip done using wildecard 
for k,v in df_by_cat.items():
	tmp_category='_'
	if k!='nocat':
		printlog.addLine( ('Saving category',k,len(v.keys())) ) # shape = (rows, columns)
		tmp_category='_'+str(k).replace('.','dot').replace('%','prct')
	
	for k2,v2 in v.items(): 
		df_shape=v2.shape
		df_save=v2
		if len(selected_columns)>0:
			try:
				df_save=v2[selected_columns]
			except: 
				printlog.addLine( (traceback.format_exc()) )
				printlog.addLine( ('??? Wrong column name in selected columns?') )
				exit()
			 
		# next_df_file=os.path.join('results',orig_file_name+tmp_category+'-items-'+ k2 )
		# nextfileFullPath=os.path.join(subdir_result, fp.file_name+tmp_category+'-items-'+ k2 )
		nextFileName= fp.file_name+tmp_category+'-items-'+ k2
		if k=='nocat':
			printlog.addLine( ('saving file',next_df_file) )
		
		tmp=fop.save_df(nextFileName,subdir_result,df_save,cur_cfg["save_column_names"],zipo,only_save_zip=False) # zip later 
		if tmp!='':
			path_to_zip.append(tmp)
	

if zipo.zipOn and  cur_cfg["zip"] :
	zip_pattern_path=os.path.join(subdir_result,'*.csv')
	printlog.addLine( ('*** Zipping files ',  '"'+ zip_pattern_path+'"') )
	
	if zipo.zip_files( subdir_result , zip_pattern_path ):
	
		for pp in path_to_zip: # list to big to zip so remove separate 
			os.remove(pp) # print('removing',pp)
			
		os.rmdir(subdir_result)
		
	printlog.addLine( ('Resulting files ready in archive ',  '"'+ subdir_result+'.zip"') )
else:
	printlog.addLine( ('Resulting files ready in  ',  '"'+ subdir_result+'"') )
		
