# this config will be read cleaned of comments as json
# comments explain config functionality

config_dict= {

	# general settings - always required 
	
	"filename":  'test.csv' , # example path in starting folder 
	"selected_columns_to_save": ['id','dates','category'] ,
	"split_category_column": 'category', # files may be created by splitting value in selected column - each unique value will be a category and a new file
	"max_rows_per_file": 32000, # if categories are off wil create fils based only on max rows, if on - will create multiple files per category when exceeding this max value 
	"save_column_names": True, # if you want column names in resulting file; if false will add col names in file name up to 128 chars
	"zip":True, # if no 7zip installed will do tar archive if set True; False will simply create files with no archive


	# csv settings only needed when processing csv file
	
	"csv_dates_columns_to_parse": ['dates'],
	"csv_us_format_day_first": False,				# if dates are US format day first is False eg 12/30/2022
	"csv_delimiter": ',' ,
	
	# if you want numeric column read as string / object you can define here as 'str' 
	"csv_dtype": {'id':"str"} # optional {'colname':'type',...} example: {'c1':'str','c2':'int', 'c3':'float'}

}