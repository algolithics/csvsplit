scvsplit app allows to split big csv file to many files according to config.py

Also if you search how to split excel file to multiple csv files this is the app to do so. Only first sheet of xlsx file will be processed.

For simplicity compiled version csvsplit.exe file is available, virus tested here: https://www.virustotal.com/gui/url-analysis/u-4cd5d0f87cc5423856b16534e9ed4a77b76c766a147e0d6a7d1110ed6c87ba96-1670175356

To use csvsplit.exe file you still need config.py in the same dir as exe file. Double click and it will process config and create output.
Logs will also be created to view potential errors.
The big file to be processed is prefered to be in the same directory.

See config file "config.py" and modify to split your file according to need.

Basic split options are :
1. by max number of rows (e.g. if you have a huge file and need a couple of small ones.
2. by category (if you have category in one column and need to split each category to a different file.

Additional options allow to select only some columns from the xls/csv file and to write also with no column names/header
